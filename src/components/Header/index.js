import React from "react";
 
const Header = ({logo}) => {
    return (
         <header className="header page-header">
            <a href="#" className="header-logo">
                <img src={logo} alt="Logo" />
            </a>
        </header>
    );
}
 
export default Header;