import React from "react";
import ReviewsListItem from "../ReviewsListItem";

const ReviewsList = ({ reviews}) => {

    return (
        <ul
            className="reviews-list"
        >
            {reviews.map((review, index) => {
                const {id, traveledWith, entryDate, travelDate, titles:{nl: title}, texts:{nl:text}, user,  ratings : { general: { general: generalRating }, aspects}} = review;
                return (
                    
                    <ReviewsListItem
                        key={index}
                        id={id}
                        traveledWith={traveledWith}
                        entryDate={entryDate}
                        travelDate={travelDate}
                        title={title}
                        text={text}
                        user={user}
                        generalRating={generalRating}
                        aspects={aspects}
                    />
                );
            })}
        </ul>

    );
};

export default ReviewsList;
