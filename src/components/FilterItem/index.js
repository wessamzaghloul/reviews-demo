import React from 'react'
import PropTypes from 'prop-types'

const FilterItem = ({ active, name, onClick }) => (
  <button
    onClick={()=>{onClick(name)}}
    disabled={active}
  >
    {name}
  </button>
)

FilterItem.propTypes = {
  active: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default FilterItem