import React from "react";

const ReviewsListItem = ({ id,
    traveledWith,
    entryDate,
    travelDate,
    title,
    text,
    user,
    generalRating,
    aspects }) => {
        function formatReviewLabel(label) {
            return label.split(/(?=[A-Z])/).join(" "); 
        }
    return (
        <li className="review-item">
        
           
                <div className="review-item__header">
                    <span className="review-item__rate">
                        {generalRating}
                        </span>
                    <span className="review-item__title">
                        {title}
                    </span>
                    <span>
                        {user}, {entryDate}
                    </span>
                </div>
               
                <div className="review-item__body">
                    <ul className="review-item__reviews">
                 
                        {
                            Object.keys(aspects).map( (key, item) =>{
                                return <li key={key}> {formatReviewLabel(key)}: {aspects[key]} </li>
                            })
                        }
                    </ul>
                    <p className="review-item__text">{text}</p>
                </div>
        
                <div className="review-item__footer">Traveled with {traveledWith} in {travelDate}</div>
      
        </li>
    );
};

export default ReviewsListItem;
