import React from "react";
import ReactDOM from "react-dom";
import "babel-polyfill";
import { Provider } from 'react-redux';
import store from "./store"
import Header from "./components/Header";
import logo from "./assets/images/logo.svg";
import Reviews from "./containers/Reviews";
import "./index.scss";


const App = () => (
    <div className="page">
        <Header logo={logo} />
        <main className="page-content">
            <Reviews/>
        </main>
    </div>
);
ReactDOM.render( 
    <Provider store={store}>
         <App />
    </Provider>
   
, document.getElementById("app"));