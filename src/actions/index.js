import fetch from 'cross-fetch';

const apiUrl = "https://api.myjson.com/bins/qxutp";

export const fetchReviews = () => {
    return dispatch => {
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => dispatch({
                type: 'FETCH_REVIEWS',
                data
            }))
    }
}

export function setFilter(filter) {
    return {
        type: 'SET_FILTER',
        filter
    }
}

export function setSortBy(sortBy) {
    return {
        type: 'SET_SORT_BY',
        sortBy
    }
}