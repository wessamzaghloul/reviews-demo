export const initialState = {
    reviews: [],
    displayedReviews:[],
    filtersList: [],
    isLoading: true,
    sortBy: "entryDate",
    activeFilter: null
};
const reviews = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_REVIEWS':
            let filtersList = [...new Set(action.data.map(({
                traveledWith
            }) => traveledWith))]
            return {
                ...state,
                reviews: action.data,
                displayedReviews: sortBy(action.data, state.sortBy),
                    filtersList: filtersList,
                    isLoading: false
            }
        case 'SET_FILTER':

            const displayedReviews = state.reviews.filter(review => review.traveledWith === action.filter);
            return{
                ...state,
                activeFilter: action.filter,
                displayedReviews: displayedReviews
            }
            case 'SET_SORT_BY':
                    return{
                        ...state,
                        sortBy: action.sortBy,
                        displayedReviews: sortBy(state.reviews, action.sortBy)
                    }
        default:
            return state;
    }
};

function sortBy(array, property){
    console.log(property)

    const sorted = array.sort((a, b) => {
        return new Date(a[property]) - new Date(b[property]);
    });
    return sorted;
}
export default reviews;