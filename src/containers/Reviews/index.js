import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchReviews, setFilter, setSortBy } from "../../actions";
import FilterItem from '../../components/FilterItem';
import ReviewsList from "../../components/ReviewsList";
import Loader from "../../components/Loader";

class Reviews extends Component {
    componentDidMount() {
        this.props.fetchReviews();
    }
    render() {
        const { displayedReviews, activeFilter,filtersList, setFilter, isLoading, setSortBy, sortBy } = this.props;
        return (
            <section className="component component-reviews">
                {isLoading ? <Loader /> : ""}
                <h1 className="page-title">Reviews</h1>
                <h6 className="page-subtitle">Reviews List</h6>
                Traveled with:
                {filtersList.map((filter, index) => {
                return (
                    <FilterItem key={index} active={activeFilter === filter} onClick={setFilter} name={filter}></FilterItem>
                );
                

            })}
            <div>
                Sort By: 
                <select onChange={(event) => {
   setSortBy(event.target.value)
  }} value={sortBy}>
                    <option value="entryDate"> Submission date </option>
                    <option value="travelDate"> Travel Date </option>
                </select>
            </div>
                
                <ReviewsList reviews={displayedReviews}/>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return state.reviews;
  }
  const mapDispatchToProps = (dispatch) => ({
    fetchReviews: () => dispatch(fetchReviews()),
    setFilter: (filter) => dispatch(setFilter(filter)),
    setSortBy: (sortBy) => dispatch(setSortBy(sortBy))
  });
  export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
